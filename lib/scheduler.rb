require 'sequel'
require 'yaml'
require "sneakers"
require "json"

conf = YAML.load_file("config/database.yml")["production"]
conf.merge!("database" => "crystal")
amqp_options = {:amqp => 'amqp://guest:guest@127.0.0.1:5672',
  :vhost => '/',
  :exchange => 'sneakers',
  :exchange_type => :direct,
  :durable => true,
  :heartbeat => 2}
Sneakers.configure :log => STDOUT
pub = Sneakers::Publisher.new(amqp_options)

db = Sequel.mysql2(conf)
#ds = db.fetch("select `database`, hostname from `databases` inner join hosts on hosts.id = (select hosts.slave_id from hosts where id = `databases`.host_id) order by `database`;")
#ds = %w(crystal_development crystal_development2 crystal_development3 crystal_development4 crystal_development5)
#ds.each do |row|
#  puts "***********************************"
#  puts row.inspect
#row = {:database=>"crystal_development", :hostname=>"127.0.0.1"}
row = {:database=>"wildthingsgames", :hostname=>"208.85.150.91"}
 pub.publish({:customer => row[:database], :host => row[:hostname]}.to_json, {:to_queue => "default"})
  #pub.publish(row, {:to_queue => "default"})
#end
