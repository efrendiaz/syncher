require "syncher/version"
require "sneakers"
require "sequel"
require "mongo"
require "json"
require "yaml"

module Syncher
  class MySyncher
    include Sneakers::Worker
    from_queue :default,
               :timeout_job_after => 1200
    def work(payload)
      parsed_payload = JSON.parse(payload)
      customer = parsed_payload["customer"]
      host = parsed_payload["host"]
      conf = YAML.load_file("config/database.yml")["production"]
      conf.merge!("database" => customer, "host" => host)
      #client = Mongo::MongoClient.from_uri('mongodb://efren:efrencc@ds029083-a0.mongolab.com:29083/lattice_efren')
      client = Mongo::MongoClient.new
      db = client.db("lattice_efren")
      coll = db.collection("inventories")
      db = Sequel.mysql2(conf)
      ds = db.fetch("select hive_product_id, variants.id as variant_id, qty, descriptors.description as description, variant_descriptors.value as value from variants, products, descriptors, variant_descriptors WHERE variants.product_id = products.id AND variant_descriptors.variant_id = variants.id AND variant_descriptors.descriptor_id = descriptors.id")
      #{:hive_product_id=>30647, :id=>37161, :qty=>20, :description=>"Condition", :value=>"Brand New"}
      puts "Processing #{ds.count} records for #{customer}"
      real_inventory = 0
      ds = ds.extension(:pagination)
      ds.each_page(100000) do |rows|
        bulk = coll.initialize_ordered_bulk_op
      	rows.each do |row|
          bulk.insert(row.merge(:client => customer))
      	end
         puts bulk.execute
       end
      puts "done with #{customer}"
      ack!
    end
  end
end
